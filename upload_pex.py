import asyncio
import telegram
import glob

async def main():
    bot = telegram.Bot(token='5913147979:AAF5zeU3PbL7aT7tOAUAXgI90y6GCUxUxrU')
    chat_id = -1001604942536
    file_pattern = 'out/target/product/violet/PixelExtended*.zip'
    file_paths = glob.glob(file_pattern)

    if file_paths:
        file_path = file_paths[0]
    with open(file_path, 'rb') as file:
        await bot.send_document(chat_id=chat_id, document=file)

asyncio.run(main())