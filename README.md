# Violet Upload Script #

### Install ###
```bash
pip install python-telegram-bot
```

### PixelExtended ###
```bash
python <(curl -s https://gitlab.com/poisonthepower/sawerwer3w/-/raw/main/upload_pex.py)
```

### CherishOS ###
```bash
python <(curl -s https://gitlab.com/poisonthepower/sawerwer3w/-/raw/main/upload_cherish.py)
```

### BananaDroid GAPPS ###
```bash
python <(curl -s https://gitlab.com/poisonthepower/sawerwer3w/-/raw/main/upload_Gbanana.py)
```

### BananaDroid VANILLA ###
```bash
python <(curl -s https://gitlab.com/poisonthepower/sawerwer3w/-/raw/main/upload_Vbanana.py)
```

### SparkOS ###
```bash
python <(curl -s https://gitlab.com/poisonthepower/sawerwer3w/-/raw/main/upload_spark.py)
```

### Requirment ###
Ubuntu or Debian
